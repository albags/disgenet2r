\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Background}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Installation}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}\texttt {DataGeNET.Dis object}}{2}{subsection.1.3}
\contentsline {section}{\numberline {2}DisGeNET and \texttt {disgenet2r}}{4}{section.2}
\contentsline {section}{\numberline {3}Retrieve gene-disease associations (GDAs) from disgenet2r}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Disease Mapping}{4}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Using a string as query}{4}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Ontology to Disease and vice-versa}{5}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Ontology class to Disease and vice-versa}{5}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Phenotype to Disease and vice-versa}{6}{subsubsection.3.1.4}
\contentsline {subsection}{\numberline {3.2}Using disease as a query}{7}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Using as a query a single disease}{7}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Plotting results of a Single Disease Query}{8}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Using as a query a list of diseases}{10}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Ploting the results of the query using a list of disease}{11}{subsubsection.3.2.4}
\contentsline {subsection}{\numberline {3.3}Using genes as a query}{14}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Using as a query a single gene}{15}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Ploting the results of a Single Gene Query}{16}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Using as a query a list of genes}{18}{subsubsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.4}Ploting the results of the query using a list of genes}{19}{subsubsection.3.3.4}
\contentsline {section}{\numberline {4}Retrieve disease-disease associations from disgenet2r}{22}{section.4}
\contentsline {subsection}{\numberline {4.1}Using disease as a query}{22}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Plotting associated disease network}{23}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Comorbidity Analysis}{25}{subsection.4.3}
