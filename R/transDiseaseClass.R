transDiseaseClass <- function( results ) {
    t( data.frame( apply( results, 1, function( row ) {
      c2.name <- row[ 1 ]
      c1.cui <- row[ 2 ]
      c1.name <- row[ 3 ]
      diseaseClass <- strsplit( gsub("; ", ";", row[ 4 ]), ";" )[[ 1 ]]
      sapply( diseaseClass, function( x ) {
        c( c2.name, c1.cui, c1.name, x )
      } )
    })))
}