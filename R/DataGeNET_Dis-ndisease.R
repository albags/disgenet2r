#' Obtain the number of unique diseases in a \code{DataGeNET.Dis}.
#'
#' @name ndisease
#' @rdname ndisease-methods
#' @aliases DataGeNET.Dis-methods
#' @param object Object of class \code{DataGeNET.Dis}
#' @return The number of unique diseases
#' @examples
#' \dontrun{
#' #Being x an DataGeNET.Dis
#' nd <- ndisease(x) # Get number of unique diseases
#' }
#' @export
setMethod( "ndisease",
  signature = "DataGeNET.Dis",
  definition = function( object ) {
    return( length( unique( object@qresult$c1.cui ) ) )
  }
)
