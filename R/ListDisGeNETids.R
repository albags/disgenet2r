disGeNETgeneIds <- function( database ) {
  
  oql <- "DEFINE
  c0='/data/gene_to_associated_diseases',
	c1='/data/genes',
	c2='/data/gene_to_gene_number',
	c4='/data/sources'
  ON
	  'http://www.disgenet.org/web/DisGeNET'
  SELECT
	c1 (geneId, name, uniprotId, description ),
	c0 (numberOfassocDiseases, sourceId)
  FROM
	c0
  WHERE
	c4 = 'DB'
  ORDER BY
	c0.numberOfassocDiseases DESC" 
  
  oql <- stringr::str_replace(
    string      = oql,
    pattern     = "DB",
    replacement = database 
  )
  
  dataTsv <- RCurl::getURLContent( 
    getUrlDis(), 
    readfunction  = charToRaw(oql), 
    upload        = TRUE, 
    customrequest = "POST"
  )
  
  data <- read.csv( textConnection( dataTsv ), header = TRUE, sep = "\t" )
  genes <- data[ !duplicated( data[ , 1] ), ]
  genes <- genes[ , c( 1:4 ) ]
  return ( genes )
}


disGeNETdiseaseIds <- function( database ) {

  oql <- "DEFINE
  c0='/data/disease_to_associated_genes',
	c1='/data/diseases',
	c2='/data/disease_to_disease_number',
	c3='/data/sources'
  ON
  'http://www.disgenet.org/web/DisGeNET'
  SELECT
	c1 (diseaseId, cui, name, omimInt, MESH, diseaseClassName, STY),
	c0 (numberOfassocGenes, sourceId)
  FROM
	c0
  WHERE
	c3 = 'DB'
  ORDER BY
	c0.numberOfassocGenes DESC" 
  
  oql <- stringr::str_replace(
    string      = oql,
    pattern     = "DB",
    replacement = database 
  )
  
  dataTsv <- RCurl::getURLContent( 
    getUrlDis(), 
    readfunction  = charToRaw(oql), 
    upload        = TRUE, 
    customrequest = "POST"
  )
  
  data <- read.csv( textConnection( dataTsv ), header = TRUE, sep = "\t" )
  diseases <- data [ !duplicated( data[ , 1] ), ]
  diseases <- diseases[ , c( 1:5) ]
  return( diseases )
}


disGeNETvariantIds <- function( database ) {
  
  oql <-  "DEFINE
	c0='/data/gene_disease_summary',
	c1='/data/diseases',
	c2='/data/genes',
	c3='/data/snpid',
	c5='/data/sources'
ON
	'http://www.disgenet.org/web/DisGeNET'
SELECT
	c0 ( score),
	c1 (cui, name, diseaseClassName),
	c2 (geneId, name, pantherName ),
	c3 (snpId)
FROM
	c3
WHERE
	(
    c5.sourceId = 'DB'
    )
      ORDER BY
    	c0.score DESC" 
 
  oql <- stringr::str_replace(
    string      = oql,
    pattern     = "DB",
    replacement = database 
  )
  
  dataTsv <- RCurl::getURLContent( 
    getUrlDis(), 
    readfunction  = charToRaw(oql), 
    upload        = TRUE, 
    customrequest = "POST"
  )
  
  data <- read.csv( textConnection( dataTsv ), header = TRUE, sep = "\t" )
  variants <- data
  #variants <- data[ !duplicated( data[ , 1] ), ]
  #variants <- variants[ , c( 1 ) ]
  return ( variants )
}