#' Query for given ontology identifier and generates an \code{DataGeNET.RDF}
#'
#' Given the ontology identifier of one or multiple disease and retrives their 
#' mapping information according to the ontology and create an object of type 
#' \code{DataGeNET.RDF}.
#'
#' @param ontologyId Name or vector of names of ontology id
#' @param ontology   The type of ontology of the id. Set it to one of the available
#' ontologyes: \code{omim} (OMIM), \code{mesh} (MeSH), \code{doid} (Disease Ontology), 
#' \code{ncit} (NCI thesaurus), \code{orphanet} (Orphanet), 
#' \code{decipher} (DECIPHER), \code{icd9cm} (ICD9CM) or \code{hp} (HP). 
#' @param verbose By default \code{FALSE}. Change it to \code{TRUE} to get a
#' on-time log from the function.
#' @param warnings By default \code{TRUE}. Change it to \code{FALSE} to don't see
#' the warnings.
#' @return An object of class \code{DataGeNET.RDF}
#' @examples
#' ont2dis <- ontology2disease( ontologyID = "D012174", ontology = "mesh" )
#' @export ontology2disease


ontology2disease <- function( ontologyID = "D012174", ontology = "mesh", verbose = FALSE, warnings =TRUE ) {
  
  # this function, gives you all mappings to disease(s) in DisGeNET, ie UMLS CUI, from an specific disease ontology ID.
  if( verbose ) {
    message( "Starting querying DisGeNET for the disease code ", ontologyID, " from the ", ontology , " ontology." )
  }
  
  # endpoint 
  endpoint <- "http://rdf.disgenet.org/sparql/"
  
  # oql is the query that let us look for a disease in DisGeNET
  oql <- "SELECT DISTINCT ?mappingID ?umlsCUI ?mapping ?disease  WHERE {  
  ?disease skos:exactMatch ?mapping .  
  ?disease rdf:type ncit:C7057 .  
  filter regex(?disease, 'umls') . 
  filter regex(str(?mapping), '^ONTOLOGYID_URI$') .
  ?disease dcterms:identifier ?umlsCUI . 
  ?mapping dcterms:identifier ?mappingID
} "
  
  # disease
  # input: query=single, namespace=UMLS. Parse input to substract 'namespace:'.
  ontologies = c("omim","mesh","doid","ncit","orphanet","decipher","icd9cm","hp")
  identifiersOntologies = c("omim","ncit","orphanet")
  namespace = tolower(ontology)
  if (namespace %in% ontologies){
    if (namespace == 'mesh') {
      namespaceURI = "http://identifiers.org/mesh.2013/"
      ontologyID_URI = paste0(namespaceURI,ontologyID)
      oql <- stringr::str_replace(
        string = oql,
        pattern = 'ONTOLOGYID_URI',
        replacement = ontologyID_URI
      )
    } else if (namespace %in% identifiersOntologies){
      ontologyID_URI = paste0("http://identifiers.org/",namespace,"/",ontologyID)
      oql <- stringr::str_replace(
        string = oql,
        pattern = 'ONTOLOGYID_URI',
        replacement = ontologyID_URI
      )
    } else if (namespace == 'doid'){
      ontologyID_URI = paste0("http://identifiers.org/",namespace,"/DOID:",ontologyID)
      oql <- stringr::str_replace(
        string = oql,
        pattern = 'ONTOLOGYID_URI',
        replacement = ontologyID_URI
      )
    } else if (namespace == 'decipher'){
      namespaceURI = "https://decipher.sanger.ac.uk/syndrome/"
      ontologyID_URI = paste0(namespaceURI,ontologyID)
      oql <- stringr::str_replace(
        string = oql,
        pattern = 'ONTOLOGYID_URI',
        replacement = ontologyID_URI
      )
    } else if (namespace == 'icd9cm'){
      namespaceURI = "http://purl.bioontology.org/ontology/ICD9CM/"
      ontologyID_URI = paste0(namespaceURI,ontologyID)
      oql <- stringr::str_replace(
        string = oql,
        pattern = 'ONTOLOGYID_URI',
        replacement = ontologyID_URI
      )
    } else if (namespace == 'hp'){
      namespaceURI = "http://purl.obolibrary.org/obo/HP_"
      ontologyID_URI = paste0(namespaceURI,ontologyID)
      oql <- stringr::str_replace(
        string = oql,
        pattern = 'ONTOLOGYID_URI',
        replacement = ontologyID_URI
      )
    }     
    
    # perform the SPARQL query to DisGeNET
    prefix <- c("sio","http://semanticscience.org/resource/","skos","http://www.w3.org/2004/02/skos/core#")
    message("Performing the query please wait...")
    res <- tryCatch({ 
      SPARQL(url=endpoint,query=oql,ns=prefix)
    },
    error = function(err){
      print(paste0("The disease with code ", ontologyID, " in the ",ontology," ontology does not map to any disease in DisGeNET. The error: "),err)
    })
    if (length(res$results) == 0){
      warning("The disease with code ", ontologyID, " in the ",ontology," ontology does not map to any disease in DisGeNET.")
    } else {
      
      result <- new( "DataGeNET.RDF", 
                     input     = ontologyID, 
                     search    = "disease", 
                     selection = ontology, 
                     mapping   = nrow(res$result), 
                     qresult   = res$result 
      )
      return( result )

    }
  }else{
    warning("The ontology '", ontology, "' introduced is not in the database. 
              Please, try again with one of the following namespaces: 
              OMIM -> 'omim', 
              MeSH -> 'mesh', 
              Disease Ontology -> 'doid', 
              NCI thesaurus -> 'ncit', 
              Orphanet -> 'orphanet', 
              DECIPHER -> 'decipher', 
              ICD9CM -> 'icd9cm',
              HP -> 'hp'.
              The ontology label introduced is not case sensitive. Thanks!")
  }
  }
