% Generated by roxygen2 (4.1.1): do not edit by hand
% Please edit documentation in R/DataGeNET_Dis-ndisease.R
\docType{methods}
\name{ndisease}
\alias{DataGeNET.Dis-methods}
\alias{ndisease}
\title{Obtain the number of unique diseases in a \code{DataGeNET.Dis}.}
\usage{
\S4method{ndisease}{DataGeNET.Dis}(object)
}
\arguments{
\item{object}{Object of class \code{DataGeNET.Dis}}
}
\value{
The number of unique diseases
}
\description{
Obtain the number of unique diseases in a \code{DataGeNET.Dis}.
}
\examples{
\dontrun{
#Being x an DataGeNET.Dis
nd <- ndisease(x) # Get number of unique diseases
}
}

