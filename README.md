# disgenet2r

`disgenet2r` is an R package for query DisGeNET database (www.disgenet.org) and visualize the results within R framework.
The disgenet2r data belongs to the last release DisGeNET v4.0 (April, 2016).

## What is this repository for?

This report is used for package distribution and testing while we end to prepare it to publish it BioConductor.

## Package' Status

 * __Version__: 0.99.0
 * __Authors__: Alba Gutierrez-Sacristan (GRIB-UPF), Janet Piñero (GRIB-IMIM), Núria Queralt-Rosinach (GRIB-IMIM)
 * __Maintainer__: Alba Gutierrez-Sacristan (GRIB-UPF)

## How to start

### Installation

The package, `disgenet2r` can be installed using `devtools` from this repository:

```R
library(devtools)
install_bitbucket("albags/disgenet2r")
```

### Querying DisGeNET:

The following lines show two examples of how DisGeNET can be queried using `disgenet2r`:

 * __Gene Query__

```R
library(disgenet2r)
gq <- disgenetGene(gene = 3953, 
    database = "ALL", 
    score = c(">", 0.1)
)
```

 * __Disease Query__

```R
library(disgenet2r)
dq <- disgenetDisease(disease = "umls:C0028754", 
    database = "ALL",
    score = c('>', 0.3) 
)
```